# Avito Notifier Bot ![version](https://img.shields.io/badge/version-0.1.0-green.svg)

Avito Notifier Bot is a telegram bot for instant notification of new arrivals of flats to [Avito](http://avito.ru/).

## Installation

### Requirements
* Linux or Mac OS
* Python 3.3 and up
* [requirements.txt](./requirements.txt)

`$ pip3 install -r requirements.txt`

## Usage

1. Register new telegram bot with [@BotFather](https://t.me/BotFather)
2. Get telegram token of your bot from [@BotFather](https://t.me/BotFather) (something like `XXX:YYYY`)
3. Start new chat with bot
4. Go to following url: `https://api.telegram.org/botXXX:YYYY/getUpdates` (replace `XXX:YYYY` with your bot token)
5. Look for your chat id
6. Run tool

```shell
usage: bot.py [-h] --chatId CHATID --telegramToken TELEGRAMTOKEN --baseUrl
              BASEURL [--maxSleepTime MAXSLEEPTIME]

This tool is used for parsing avito.com and instant notification of new flats
arrivals

optional arguments:
  -h, --help            show this help message and exit
  --chatId CHATID       the id of Telegram chat
  --telegramToken TELEGRAMTOKEN
                        telegram token
  --baseUrl BASEURL     base url for avito page with filter
  --maxSleepTime MAXSLEEPTIME
                        max time between avito checks
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](./LICENSE)
